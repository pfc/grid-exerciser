#!/bin/bash -e
# Checks a DAG and its jobs are running ok

IGWN_POOL=osg-ligo-1.t2.ucsd.edu

# Check DAG is in the queue
dagmanjob=$(condor_q -pool ${IGWN_POOL} -constraint '(JobBatchName=="grid-exerciser") && (Cmd=="/usr/bin/condor_dagman")' -af ClusterID)

if [ ! -z ${dagmanjob} ]; then # If DAG is in the queue, check the status of any child jobs still in the queue

    echo -e "DAG detected in queue with clusterID $dagmanjob\n"

    # Loop through all child nodes of the DAG
    # FIXME: this will include the dagman process itself.  Is this what we want?
    for cluster in $(condor_q -pool ${IGWN_POOL} -constraint "DAGManJobID==${dagmanjob}" -af ClusterID | sort | uniq); do

        # Loop through cluster processes
        for proc in $(condor_q -pool ${IGWN_POOL} -constraint "ClusterID==${cluster}" -af ProcID); do

            cmd=$(condor_q -pool ${IGWN_POOL} ${cluster}.${proc} -af Cmd)

            echo "---------------------------------------------------"
            echo "Checking job ${cluster}.${proc} in DAG ${dagmanjob}"
            echo "Cmd: ${cmd}"

            dagnodename=$(condor_q -pool ${IGWN_POOL} ${cluster}.${proc} -af DAGNodeName)

            echo "Checking process ${proc} in cluster ${cluster} [${dagnodename}]"

            job_status=$(condor_q -pool ${IGWN_POOL} ${cluster}.${proc} -af JobStatus)

            # We could do this as a periodic_remove but I'd like the alert from
            # the CI failing if it's sat for too long
            entered_current_status=$(condor_q ${cluster}.${proc} -af EnteredCurrentStatus)
            time_in_status=$(( $(date +%s) - ${entered_current_status}))

            # TODO: move case check into a function to run on dagman process
            case "$job_status" in

                0) echo "${cluster}.${proc} Unexpanded, U"
                    # Probably not good
                    ;;
                1) echo "${cluster}.${proc} idle, I"

                    echo "Idle, checking if timeout exceeded"
                    if [ ${time_in_status} -gt 1800 ]; then
                        echo "Idling for > 1000s, test failed"
                        condor_rm ${cluster}.${proc}
                    fi

                    ;;
                2) echo  "${cluster}.${proc} running, R"
                    # Nothing to do
                    echo "Running, checking if timeout exceeded"
                    if [ ${time_in_status} -gt 1800 ]; then
                        echo "Idling for > 5000s, test failed"
                        condor_rm ${cluster}.${proc}
                    fi
                    ;;
                3) echo  "${cluster}.${proc} removed, X"
                    # Not good
                    ;;
                4) echo  "${cluster}.${proc} completed, C"
                    # nothing to do
                    continue
                    ;;
                5) echo  "${cluster}.${proc} held, H"
                    # Probably bad, do something
                    condor_rm ${cluster}.${proc}
                    ;;
                6) echo "${cluster}.${proc} submission error, E"
                    ;;
            esac

        done
    done

else # exit with the DAG status
    dag_status=$(condor_history -constraint 'regexp("grid-exerciser.dag.*", JobBatchName) && (Cmd=="/usr/bin/condor_dagman")' -limit 1 -af DAG_Status)
    echo "DAG not detected, most recent DAG_Status: ${dag_status}"
    if [ ${dag_status} -eq 4 ]; then
      iwd=$(condor_history -constraint 'regexp("grid-exerciser.dag.*", JobBatchName) && (Cmd=="/usr/bin/condor_dagman")' -limit 1 -af Iwd)
      njobs=$(cat ${iwd}/*metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs'])")
      jobs_failed=$(cat ${iwd}/*metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs_failed'])")
      jobs_succeeded=$(cat ${iwd}/*metrics |  python3 -c "import sys, json; print(json.load(sys.stdin)['jobs_succeeded'])")
      echo "Jobs Failed: ${jobs_failed}/${njobs}"
      echo "Jobs Failed: ${jobs_succeeded}/${njobs}"
      exit ${jobs_failed}
    else
      exit ${dag_status}
    fi
fi


