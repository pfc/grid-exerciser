#!/bin/bash -e
# Parses copies of jobs' history classads for important details of how a job went

echo "# UnixEpoch DAGNodeName Cmd Site WallTime UserCpuTime SysCpuTime ExitCode" >> summary.txt

for classad in $(find $1 -type f); do

    # Check for classad file
    if [ -f ${classad} ]; then

        # Job details
        dagnodename=$(grep DAGNodeName ${classad} | cut -d= -f2 | tr -d '"' | tr -d '[:space:]')
        cmd=$(grep Cmd ${classad} | cut -d= -f2 | tr -d '"' | xargs basename | tr -d '[:space:]')

        # Report site
        site=$(grep -e "MATCH_GLIDEIN_Site[[:space:]]" ${classad} | cut -d= -f2 | tr -d '[:space:]' | sed -e 's/^"//' -e 's/"$//')
        if [ -z ${site} ]; then
            site="Unknown"
        fi

        # Report WallTime
        walltime=$(grep RemoteWallClockTime ${classad} | cut -d= -f2 | tr -d '[:space:]')
        usertime=$(grep CumulativeRemoteUserCpu ${classad} | cut -d= -f2 | tr -d '[:space:]')
        systime=$(grep CumulativeRemoteSysCpu ${classad} | cut -d= -f2 | tr -d '[:space:]')

        # Check for exit code
        exitcode=$(grep ExitCode ${classad} | cut -d= -f2 | tr -d '[:space:]')


echo -e "DAGNodeName: ${dagnodename}
Cmd: ${cmd}
Site: ${site}
WallTime: ${walltime}
UserCpuTime: ${usertime}
SysCpuTime: ${systime}
ExitCode: ${exitcode}"

        echo "$(date +%s) ${dagnodename} ${cmd} ${site} ${walltime} ${usertime} ${systime} ${exitcode}" >> summary.txt

        if [ "${exitcode}" -ne "0" ]; then
            echo $exitcode
            jobs_failed=1
        fi

    else
        echo "MISSING HISTORY CLASSAD"
        exit 1
    fi

done

if [ ! -z ${jobs_failed} ]; then
    echo "JOB FAILURES PRESENT"
    exit 1
fi
