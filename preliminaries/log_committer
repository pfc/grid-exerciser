#!/bin/bash -e
TOKEN_FILE=$1
JOB_NAME=$2
OUTPUT_FILES=${@:3}

# git configuration
token=$(cat ${TOKEN_FILE})
repo=https://condor:${token}@git.ligo.org/james-clark/igwn-osg-mon.git
git clone ${repo}

# Results from this job
jobdir=results/${JOB_NAME}/$(date +%Y%m%d_%H%M%S)
mkdir -p igwn-osg-mon/${jobdir}

# Commit job output, log
for ext in out err log; do
    file=${JOB_NAME}.${ext}
    if [ -f ${file} ]; then
        # Get ClassAd
        if [ "$ext" = "log" ]; then
            cluster=$(grep "Cluster = " ${file} | head -1 | cut -d= -f2 )
            condor_history ${cluster} -long -limit 1 > igwn-osg-mon/${jobdir}/job_history_classad
            git -C igwn-osg-mon add ${jobdir}/job_history_classad
        fi
    else
        echo "MISSING ${file}" >> igwn-osg-mon/${jobdir}/missing_output.txt
        git -C igwn-osg-mon add ${jobdir}/missing_output.txt
    fi
done

# Test for output data
for output in ${OUTPUT_FILES}; do
    if [ ! -f ${output} ]; then
        echo "MISSING ${output}" >> igwn-osg-mon/${jobdir}/missing_data.txt
        git -C igwn-osg-mon add ${jobdir}/missing_data.txt
    fi
done

# Finalise
git -C igwn-osg-mon commit -m "[condor] ${JOB_NAME}/$(date +%Y%m%d_%H%M%S)"
git -C igwn-osg-mon push

# Clean up
rm -rf igwn-osg-mon

exit 0

