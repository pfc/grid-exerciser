# grid-exerciser

Submits a DAG on a schedule to
`ldas-osg.ligo.caltech` with jobs to check grid functionality.

Parts of the HTCondor workflow trigger CI jobs to assess performance.

## TODO

* Tune time-out in `check_dag_status` to realistic walltime of DAG (currently 30 mins)
* Automate X509 authentication - keytab?
* Aggregate, plot job metrics
* Aggregate, plot DAG metrics from dagman metrics file
* Expand test jobs: explicit IGWN conda environment test
* Expand test jobs: random authenticated CVMFS frame access


## Workflow

The entire process is scheduled through the CI/CD workflow:

1. Generate files containing SSH keys and gitlab access tokens from CI
   environment variables.
1. Copy (scp gitlab -> schedd) grid exerciser workflow files (DAGMan, submit
   files, access token) to a working directory on the schedd.
1. Submit workflow (`ssh <schedd> condor_submit_dag`)
1. Each node in the DAG contains a POST script which dumps that job's history
   ClassAd to a file in the working directory and triggers the next CI job.
1. The triggered job pulls in the history classads (scp schedd -> gitlab) and
   checks for job failures.  If jobs have failed, the CI pipeline will fail and 
   we will get an email alert.  Summary information (`Cmd`, site, computation
   time, etc) from each job *process* is appended to a text file.
1. The summary information from each job is aggregated into a file for each job
   type and (will be) plotted and reported, perhaps analysed for performance
   degradation.


## Authentication

### Schedd

Jobs are submitted over ssh, with a private/public key pair generated offline
and uploaded to myligo.org.  The private key is saved as a variable in gitlab
and echoed into a key file at pipeline startup.

### Gitlab

Each DAG node runs a POST script which uploads logs to gitlab.  These use a
personal access token which, again, is echoed to file and sent out with the
jobs with permission 0600.


