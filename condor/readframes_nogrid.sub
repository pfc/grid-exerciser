universe = vanilla

## The application and arguments
batch_name = "readframes_nogrid"
executable = /cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/lalapps_frview
arguments = "--cache-file=$(cache_file) --channel=$(channel) --min-frequency=$(min_frequency) --max-frequency=$(max_frequency) --sample-rate=$(sample_rate) --output-file=$(output_file) --output-type=$(output_type) --gps-start-time=$(gps_start_time) --duration=$(duration)"

## Request resources
request_disk = 2 MB
request_cpus = 1
request_memory = 1 GB 

## Set up file transfers
should_transfer_files = YES
transfer_executable = False
transfer_input_files = $(cache_file)

# transfer_output_files =
# > transfer_output_files empty: no files
# > transfer_output_files non-existent: all (sandbox) files
transfer_output_files = $(output_file)

## Allow local cluster
+flock_local = True
+DESIRED_Sites = "nogrid"

## Stream stdout and stderr
stream_output = True
stream_error = True

## IGWN accounting information
accounting_group = cit.test

## Set up authentication: CVMFS frame access REQUIRES a VALID x509 proxy
# (e.g. ligo-prox-init)
use_x509userproxy = True

## Condor UserLog, stderr and stdout files
log = readframes_nogrid-$(cluster)_$(process).log
error = readframes_nogrid-$(cluster)_$(process).err
output = readframes_nogrid-$(cluster)_$(process).out

notification = Error
queue 1
